package com.lihuinch.test.feishu.eventsubscribe;

import com.lihuinch.test.feishu.eventsubscribe.util.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.lihuinch")
public class FeishuEventSubscribeApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext =  SpringApplication.run(FeishuEventSubscribeApplication.class, args);
        SpringContextUtil.setApplicationContext(applicationContext);

    }

}
