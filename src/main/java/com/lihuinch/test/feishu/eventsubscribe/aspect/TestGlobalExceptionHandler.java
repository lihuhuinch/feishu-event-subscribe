package com.lihuinch.test.feishu.eventsubscribe.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 。
 *
 * @author lihu
 * @date 2021-8-27 10:35
 * @since
 */
@ControllerAdvice
public class TestGlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String handleException(HttpServletRequest req, Exception e){

        logger.error(" =================== ExceptionHandler =================== ", e);

        return "error:" + e.getMessage();
    }

}
