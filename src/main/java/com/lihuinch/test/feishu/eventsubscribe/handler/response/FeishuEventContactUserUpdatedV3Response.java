package com.lihuinch.test.feishu.eventsubscribe.handler.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 。
 *
 * @author lihu
 * @date 2021-8-27 16:22
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeishuEventContactUserUpdatedV3Response extends FeishuEventResponse {
}
