package com.lihuinch.test.feishu.eventsubscribe.constant;

/**
 * 。
 *
 * @author lihu
 * @date 2021-8-24 14:36
 * @since
 */
public class TestConstants {

    public static String verificationToken = "xTgUoG5H3wJk2v9YD7LQZcNDtEvGIqWA";

    // 非必填
    public static String encryptKey = "CXmtVNyboEkn5F5fDMCkmdbzkFtcuSg7pigSiqy9uYg";

    public static class FeiShuParam {
        public static String VERIFY_URL_CHALLENGE = "challenge";
        public static String VERIFY_URL_ENCRYPT = "encrypt";

        public static String EVENT_SCHEMA = "schema";
        public static String EVENT_SCHEMA_V2 = "2.0";
        public static String EVENT_HEADER = "header";

        public static class Header {

            public static String EVENT_ID = "event_id";
            public static String TOKEN = "token";
            public static String CREATE_TIME = "create_time";
            public static String EVENT_TYPE = "event_type";
            public static String TENANT_KEY = "tenant_key";
            public static String APP_ID = "app_id";
        }

        public static String EVENT_EVENT = "event";
    }

    public static class FeishuEvent {
        public static final String VERIFY_URL = "verifyUrl";

        public static class Contact {
            public static final String CONTACT_USER_UPDATED_V3 = "contact.user.updated_v3";
        }

    }




}
