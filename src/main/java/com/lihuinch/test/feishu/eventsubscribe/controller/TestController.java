package com.lihuinch.test.feishu.eventsubscribe.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.lihuinch.test.feishu.eventsubscribe.constant.TestConstants;
import com.lihuinch.test.feishu.eventsubscribe.handler.FeishuEventHandler;
import com.lihuinch.test.feishu.eventsubscribe.feishu.FeishuSignatureUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * 。
 *
 * @author lihu
 * @date 2021-8-24 14:14
 * @since
 */
@RestController
public class TestController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = FeishuEventHandler.FEI_SHU_EVENT_HANDLER + "")
    private FeishuEventHandler feishuEventHandler;

    @PostMapping(value = "/feishu/event")
    public void validApi(@RequestHeader("X-Lark-Request-Timestamp") String timestamp,
                                        @RequestHeader("X-Lark-Request-Nonce") String nonce,
                                        @RequestHeader("X-Lark-Signature") String signature,
                                        @RequestBody String jsonStrParam) throws NoSuchAlgorithmException {

        logger.info("X-Lark-Request-Timestam  timestamp = {}", timestamp);
        logger.info("X-Lark-Request-Nonce     nonce     = {}", nonce);
        logger.info("X-Lark-Signature         signature = {}", signature);

        String calculateSignature = FeishuSignatureUtils.calculateSignature(timestamp, nonce, TestConstants.encryptKey, jsonStrParam);

        logger.info("signature          = {}, ", signature);
        logger.info("calculateSignature = {}, ", calculateSignature);

        Assert.isTrue(Objects.equals(signature, calculateSignature), "签名计算错误");


        logger.info("jsonStrParam \n{}", JSON.toJSONString(jsonStrParam, SerializerFeature.PrettyFormat));

        // 不需要返回什么 200状态足以
        feishuEventHandler.handleEvent(JSON.parseObject(jsonStrParam));
    }

    public FeishuEventHandler getFeishuEventHandler() {
        return feishuEventHandler;
    }

    public void setFeishuEventHandler(FeishuEventHandler feishuEventHandler) {
        this.feishuEventHandler = feishuEventHandler;
    }
}
