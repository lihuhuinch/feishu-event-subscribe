
/**
 * Copyright 2021 bejson.com
 */
package com.lihuinch.test.feishu.eventsubscribe.handler.request;

import lombok.Data;

/**
 * Auto-generated: 2021-08-27 16:20:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class FeishuEvenVerifyUrlParam {

    private String challenge;
    private String token;
    private String type;
}