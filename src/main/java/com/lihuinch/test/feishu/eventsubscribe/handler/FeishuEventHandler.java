package com.lihuinch.test.feishu.eventsubscribe.handler;

import com.alibaba.fastjson.JSONObject;
import com.lihuinch.test.feishu.eventsubscribe.handler.response.FeishuEventResponse;

/**
 * 。
 *
 * @author lihu
 * @date 2021-8-27 14:53
 * @since
 */
public interface FeishuEventHandler<R extends FeishuEventResponse> {

    /**
     * 子类都以这个作为springbean的开头
     */
    String FEI_SHU_EVENT_HANDLER = "feishuEventHandler";

    R handleEvent(JSONObject param);


    static String getBeanId(String event) {
        return FEI_SHU_EVENT_HANDLER + "_" + event;
    }
}
