package com.lihuinch.test.feishu.eventsubscribe.handler.impl;

import com.alibaba.fastjson.JSONObject;
import com.lihuinch.test.feishu.eventsubscribe.constant.TestConstants;
import com.lihuinch.test.feishu.eventsubscribe.handler.FeishuEventHandler;
import com.lihuinch.test.feishu.eventsubscribe.handler.response.FeishuEventContactUserUpdatedV3Response;
import com.lihuinch.test.feishu.eventsubscribe.handler.response.FeishuEventResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 用于飞书验证服务器url请求。
 *
 * @author lihu
 * @date 2021-8-27 16:16
 * @since
 */
@Service(FeishuEventHandler.FEI_SHU_EVENT_HANDLER + "_" + TestConstants.FeishuEvent.Contact.CONTACT_USER_UPDATED_V3)
public class FeishuEventContactUserUpdatedV3Handler implements FeishuEventHandler<FeishuEventContactUserUpdatedV3Response> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public FeishuEventContactUserUpdatedV3Response handleEvent(JSONObject param) {

        logger.info("FeishuEventContactUserUpdatedV3Handler");

        return new FeishuEventContactUserUpdatedV3Response();
    }
}
