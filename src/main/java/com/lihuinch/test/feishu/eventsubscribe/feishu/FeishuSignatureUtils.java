package com.lihuinch.test.feishu.eventsubscribe.feishu;

import org.apache.commons.codec.binary.Hex;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FeishuSignatureUtils {

    /**
     * https://open.feishu.cn/document/ukTMukTMukTM/uYDNxYjL2QTM24iN0EjN/event-security-verification
     *
     * @param timestamp  Http请求头中的X-Lark-Request-Timestamp
     * @param nonce      Http请求头中的X-Lark-Request-Nonce
     * @param encryptKey 飞书应用后台自己配置的encrypt key
     * @param bodyString 事件请求json串
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String calculateSignature(String timestamp,
                                            String nonce,
                                            String encryptKey,
                                            String bodyString) throws NoSuchAlgorithmException {
        StringBuilder content = new StringBuilder();
        content.append(timestamp).append(nonce).append(encryptKey).append(bodyString);
        MessageDigest alg = MessageDigest.getInstance("SHA-256");
        return Hex.encodeHexString(alg.digest(content.toString().getBytes()));
    }
}
